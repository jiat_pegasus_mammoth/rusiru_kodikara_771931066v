package com.rusiru.web.db;

import com.rusiru.web.util.ApplicationProperties;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class DBConnection {
    private static Connection connection;
    static ApplicationProperties properties = ApplicationProperties.getInstance();

    public static Statement createConnection() throws Exception {
        Class.forName(properties.get("sql.connection.driver"));
        connection = DriverManager.getConnection(properties.get("sql.connection.url"), properties.get("sql.connection.username"), properties.get("sql.connection.password"));

        return connection.createStatement();
    }
    public static void iud(String query) {
        try {
            createConnection().executeUpdate(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ResultSet search(String query) throws Exception {
        return createConnection().executeQuery(query);
    }
}
