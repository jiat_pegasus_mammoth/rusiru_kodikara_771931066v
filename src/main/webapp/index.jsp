<%--
  Created by IntelliJ IDEA.
  User: rusir
  Date: 3/30/2023
  Time: 6:06 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css" rel="stylesheet">
    <title>Rusiru Kodikara_771930166v</title>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <nav class="navbar navbar-expand-lg bg-body-tertiary">
                <div class="container-fluid">
                    <a class="navbar-brand" href="#">Rusiru Kodikara</a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                            data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false"
                            aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="#">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Features</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Pricing</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"
                                   aria-expanded="false">
                                    Homeworks
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="#">Homework 1 03/31/2023</a></li>
                                    <li><a class="dropdown-item" href="#">Homework 2</a></li>
                                    <li><a class="dropdown-item" href="#">Homework 3</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-7">
            <table class="table table-dark table-striped">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Mobile</th>
                    <th scope="col">Password</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td>Mark</td>
                    <td>Otto</td>
                    <td>@mdo</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-5">
            <div class="row">
                <div class="col-12">
                    <h1>Register a user</h1>
                </div>
                <form action="/register" method="post">
                    <div class="col-12 mt-3">
                        <label>Name</label>
                        <input type="text" name="name" class="w-100">
                    </div>
                    <div class="col-12 mt-3">
                        <label>Mobile</label>
                        <input type="text" name="mobile" class="w-100">
                    </div>
                    <div class="col-12 mt-3">
                        <label>Password</label>
                        <input type="password" name="password" class="w-100">
                    </div>
                    <div class="col-12 mt-3">
                        <button class="btn btn-primary w-100">Register</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
